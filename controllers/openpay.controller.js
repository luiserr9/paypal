var Openpay = require('openpay');
//instantiation
var openpay = new Openpay('mnp6jj7xlcndnu76npve', 'sk_e0e988171c70403ea85f125fb5cfba23')

var controller = {

    openpay: async function (req, res){


        return res.status(200).send({
            status:'success',
            mensaje: 'openpay',
            data:openpay
        })

    },
    openPayCard: async function (req, res) {

        var cardRequest = {
            'card_number':'4111111111111111',
            'holder_name':'Juan Perez Ramirez',
            'expiration_year':'21',
            'expiration_month':'12',
            'cvv2':'110'
        };

        openpay.cards.create(cardRequest, (error, card)=>{
           
            if (error == null) {
                console.log(card);
                return res.status(200).send({
                    card:card
                })
            }

            
        });
        
    },
    openpayCharges: async function( req, res) {
       

        var chargeRequest = {
            'source_id' : 'kx2s36vcyf3dllhyazhm',
            'method' : 'card',
            'amount' : 100,
            'currency' : 'MXN',
            'description' : 'Cargo inicial a mi cuenta',
            'order_id' : 'oid-00052',
            'device_session_id' : 'kR1MiQhz2otdIuUlQkbEyitIqVMiI16f',
            'customer' : {
                 'name' : 'Juan',
                 'last_name' : 'Vazquez Juarez',
                 'phone_number' : '4423456723',
                 'email' : 'juan.vazquez@empresa.com.mx'
            }
        }

        openpay.charges.create(chargeRequest, function(error, charge) {
           console.log(error);
           console.log(charge);
        });
       
    }




    
};

module.exports = controller;