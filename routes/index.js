const express = require('express'); //Cargar framework de nodejs
const router = express.Router(); //Cargar nucleo router
//controladores
const paypalController = require('../controllers/paypal.controller');
const openpayController = require('../controllers/openpay.controller');

module.exports = function () {
    router.get('/paypal-token',paypalController.generarTokenPaypal);
    router.post('/paypal-new-checkout',paypalController.generarPayoutPaypal);
    router.get('/openpay',openpayController.openpay);

    router.get('/openpay/card',openpayController.openPayCard);
    router.get('/openpay/pay',openpayController.openpayCharges);
    return router;
}
